use hyper::{
    service::{make_service_fn, service_fn},
    Body, Request, Response,
};

async fn hello(_: Request<Body>) -> Result<Response<Body>, std::convert::Infallible> {
    Ok(Response::new(Body::from("Hello World!")))
}

fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    jive::block_on(async move {
        let make_svc =
            make_service_fn(|_conn| async { Ok::<_, std::convert::Infallible>(service_fn(hello)) });

        let server = hyperjive::server::builder(([0, 0, 0, 0], 8080))
            .await?
            .serve(make_svc);

        println!("Listening on port 8080");
        server.await?;

        Ok(())
    })
}
