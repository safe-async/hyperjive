fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    jive::block_on(async {
        let client = hyperjive::client::new()?;

        let res = client.get("http://httpbin.org/ip".parse()?).await?;

        println!("GOT: {}", res.status());

        let buf = hyper::body::to_bytes(res).await?;
        let s = String::from_utf8_lossy(&buf);
        println!("{}", s);
        Ok(())
    })
}
