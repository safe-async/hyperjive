async fn hello() -> &'static str {
    "hewwo mr obama"
}

fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    jive::block_on(async move {
        let app = axum::Router::new().route("/", axum::routing::get(hello));

        hyperjive::server::builder(([0, 0, 0, 0], 8080)).await?.serve(app.into_make_service()).await?;

        Ok(())
    })
}
